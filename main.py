from fastapi import FastAPI
from dotenv import load_dotenv  # pip3 install python-dotenv
import os
import boto3
import tempfile
from keras.models import Model, load_model

app = FastAPI()

load_dotenv()

AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY = os.getenv('AWS_SECRET_KEY')
# BUCKET_NAME = os.getenv('BUCKET_NAME')
BUCKET_NAME = 'elasticbeanstalk-us-west-2-303518111465'


def get_s3():
    return boto3.client('s3',
                        aws_access_key_id=AWS_ACCESS_KEY,
                        aws_secret_access_key=AWS_SECRET_KEY)


# def s3_get_h5_model(model_name: str) -> Model:
#     with tempfile.TemporaryDirectory() as tempdir:
#         s3 = get_s3()
#         mdl_path = os.path.join(tempdir, model_name)
#         # Fetch and save the h5 file to the temporary directory
#         s3.download_file(BUCKET_NAME, 'models/' + model_name, mdl_path)
#         # Load the keras model from the temporary directory
#         return load_model(mdl_path)
#
#
# tf_model = s3_get_h5_model('model_tuner_base_aug.h5')
# ####
# with tempfile.TemporaryDirectory() as tempdir:
#     s3 = get_s3()
#     # mdl_path = f'{tempdir}/model_tuner_base_aug.h5'
#     mdl_path = os.path.join(tempdir, 'model_tuner_base_aug.h5')
#     s3.download_file(BUCKET_NAME, 'models/' + 'model_tuner_base_aug.h5', mdl_path)
#     tf2_model = load_model(mdl_path)
# ####

s3 = get_s3()

print(tempfile.gettempdir())
tmp = tempfile.mkdtemp()
mdl_path = os.path.join(tmp, 'model_tuner_base_aug.h5')
s3.download_file(BUCKET_NAME, 'models/' + 'model_tuner_base_aug.h5', mdl_path)
tf2_model = load_model(mdl_path)


@app.get("/")
async def root():
    return {"message": "Hello World"}
