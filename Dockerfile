FROM python:3.9.7
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8000
# CMD ['python', 'main.py']
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]